GO_LINT_VERSION = v1.42.1
JUNIT_VERSION = v0.9.1
LSIF_GO_VERSION = latest

lint:
	go run github.com/golangci/golangci-lint/cmd/golangci-lint@${GO_LINT_VERSION} run ./... -v --timeout 5m

lsif:
	go run github.com/sourcegraph/lsif-go/cmd/lsif-go@${LSIF_GO_VERSION}

junit:
	go test -covermode=count ./... -v 2>&1 | go run github.com/jstemmer/go-junit-report@${JUNIT_VERSION} -set-exit-code > report.xml

test:
	go test ./... -v 2>&1