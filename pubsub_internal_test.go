package pubsub

import (
	"context"
	"testing"
)

func TestPubSub_channel_closes(t *testing.T) {
	pb := New[string, int]()

	ctx, cancel := context.WithCancel(context.Background())
	primesCh := pb.Subscribe(ctx, "primes")

	channelClosedCh = make(chan struct{})
	defer func() {
		channelClosedCh = nil
	}()

	pb.Publish("primes", 3)

	cancel()
	<-channelClosedCh

	_, ok := <-primesCh
	if ok {
		t.Errorf("expected subscription channel to be closed, but it was open")
	}

}
