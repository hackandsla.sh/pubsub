package pubsub_test

import (
	"context"
	"sort"
	"testing"

	"gitlab.com/hackandsla.sh/pubsub"
)

func TestPubSub_one_pub_one_sub(t *testing.T) {
	pb := pubsub.New[string, int]()

	ctx := context.Background()
	primesCh := pb.Subscribe(ctx, "primes")

	pb.Publish("primes", 3)

	got := <-primesCh
	if got != 3 {
		t.Errorf("got %v, expected 3", got)
	}
}

func TestPubSub_no_sub(t *testing.T) {
	pb := pubsub.New[string, int]()

	pb.Publish("primes", 3)
}

func TestPubSub_multiple_subs(t *testing.T) {
	pb := pubsub.New[string, int]()

	ctx := context.Background()
	primesCh1 := pb.Subscribe(ctx, "primes")
	primesCh2 := pb.Subscribe(ctx, "primes")

	pb.Publish("primes", 3)

	got := <-primesCh1
	if got != 3 {
		t.Errorf("got %v, expected 3", got)
	}

	got = <-primesCh2
	if got != 3 {
		t.Errorf("got %v, expected 3", got)
	}
}

func TestPubSub_multiple_publish(t *testing.T) {
	pb := pubsub.New[string, int]()

	ctx := context.Background()
	primesCh := pb.Subscribe(ctx, "primes")

	pb.Publish("primes", 3)
	pb.Publish("primes", 5)

	var primes []int
	for i := 0; i < 2; i++ {
		primes = append(primes, <-primesCh)
	}

	sort.Slice(primes, func(i, j int) bool {
		return i > j
	})

	if primes[0] != 3 {
		t.Errorf("expected primes[0] to be 3, got %v", primes[0])
	}

	if primes[1] != 5 {
		t.Errorf("expected primes[1] to be 5, got %v", primes[1])
	}
}
