package pubsub_test

import (
	"context"
	"fmt"
	"time"

	"gitlab.com/hackandsla.sh/pubsub"
)

func ExamplePubSub() {
	ctx, cancel := context.WithCancel(context.Background())
	defer cancel()

	// Initialize our pub/sub channel
	pb := pubsub.New[string, int]()

	// Subscribe to the channel
	primesSub := pb.Subscribe(ctx, "primes")

	// Publish some items to the given topic
	pb.Publish("primes", 3)
	pb.Publish("primes", 5)

	// Stop subscribing after 100 milliseconds
	go func() {
		<-time.After(100 * time.Millisecond)
		cancel()
	}()

	for prime := range primesSub {
		fmt.Printf("Got prime: %v\n", prime)
	}

	// Unordered output:
	// Got prime: 3
	// Got prime: 5
}
