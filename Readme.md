> Note, this package is still experimental. Please use with caution.

# PubSub

`pubsub` is a small library to implement an idiomatic, asynchronous, and generic publisher-subscriber pattern in Go.

## Installation

```bash
go get gitlab.com/hackandsla.sh/pubsub
```

## Usage

First, initialize your `PubSub` channel, defining the topic and message types:

```go
pb := pubsub.New[string, int]()
```

Add subscribers by calling the `Subscribe` method:

```go
for message := range pb.Subscribe(ctx, "topic") {
  // Use message
}
```

Publish messages using the `Publish` method:

```go
message := 42
pb.Publish("topic", message)
```

See [the example](./examples/primes/main.go) for a basic implementation of this pattern.

## License

This code is [MIT licensed](LICENSE)
