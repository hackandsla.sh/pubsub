package pubsub

import (
	"context"
	"sync"
)

// This a channel used for testing purposes.
var channelClosedCh chan struct{} = nil

// PubSub is an in-memory, asynchronous pub/sub mechanism.
type PubSub[K comparable, V any] struct {
	subscribersMu    sync.RWMutex
	subscribers      map[K]map[int64]*subscriber[V]
	lastSubscriberID int64
}

// New initializes a new pub/sub channel.
func New[K comparable, V any]() *PubSub[K, V] {
	return &PubSub[K, V]{
		subscribers: make(map[K]map[int64]*subscriber[V]),
	}
}

// Publish asynchronously publishes a message onto the given topic. This call is
// non-blocking, and the message will be sent to the channels of all
// currently-active subscribers.
func (pb *PubSub[K, V]) Publish(topic K, msg V) {
	pb.subscribersMu.RLock()
	defer pb.subscribersMu.RUnlock()

	for _, sub := range pb.subscribers[topic] {
		sub := sub

		sub.pushMessage(msg)
	}
}

// Subscribe subscribes to the given topic. Messages will arrive asynchronously
// through the returned channel.
//
// Canceling the given context will cause the subscription channel to be
// drained and closed.
//
// No guarantees are made about the ordering of messages. For example, if a
// publisher publishes 3 messages and the subscriber takes some time handling
// the first message, the next two messages could be received in any order.
func (pb *PubSub[K, V]) Subscribe(ctx context.Context, topic K) <-chan V {
	msgCh := make(chan V)
	sub := &subscriber[V]{
		doneCh: ctx.Done(),
		msgCh:  msgCh,
	}

	subscriberID := pb.registerSubscriber(topic, sub)

	go func() {
		<-sub.doneCh
		pb.unregisterSubscriber(topic, subscriberID)

		sub.wg.Wait()

		close(sub.msgCh)

		if channelClosedCh != nil {
			close(channelClosedCh)
		}
	}()

	return msgCh
}

func (pb *PubSub[K, V]) registerSubscriber(topic K, sub *subscriber[V]) (id int64) {
	pb.subscribersMu.Lock()
	defer pb.subscribersMu.Unlock()

	pb.lastSubscriberID++
	subscriberID := pb.lastSubscriberID

	if _, ok := pb.subscribers[topic]; !ok {
		pb.subscribers[topic] = make(map[int64]*subscriber[V])
	}

	pb.subscribers[topic][subscriberID] = sub

	return subscriberID

}

func (pb *PubSub[K, V]) unregisterSubscriber(topic K, id int64) {
	pb.subscribersMu.Lock()
	defer pb.subscribersMu.Unlock()

	delete(pb.subscribers[topic], id)
}

type subscriber[V any] struct {
	wg     sync.WaitGroup
	msgCh  chan<- V
	doneCh <-chan struct{}
}

func (sub *subscriber[V]) pushMessage(msg V) {
	// Immediately return if we're done processing
	select {
	case <-sub.doneCh:
		return
	default:
	}

	// Track all messages yet-to-be-delivered
	sub.wg.Add(1)

	go func() {
		defer sub.wg.Done()

		select {
		case sub.msgCh <- msg:
		// If we're done processing messages, skip sending messages to the message
		// channel.
		case <-sub.doneCh:
		}
	}()
}
