package main

import (
	"context"
	"fmt"
	"os"
	"os/signal"
	"time"

	"gitlab.com/hackandsla.sh/pubsub"
)

func main() {

	ctx, cancel := signal.NotifyContext(context.Background(), os.Interrupt)
	defer cancel()

	pb := pubsub.New[string, int]()
	go publishPrimes(ctx, pb)

	fmt.Println("Press CTRL-C to exit")

	for prime := range pb.Subscribe(ctx, "primes") {
		fmt.Printf("Prime received: %v\n", prime)
	}

	fmt.Println("Stopping prime number generator")
}

// Publishes a prime number to the "primes" topic every second.
func publishPrimes(ctx context.Context, pb *pubsub.PubSub[string, int]) {
	i := 2
	for {
		select {
		case <-ctx.Done():
			return
		default:
		}

		i++
		if !isPrime(i) {
			continue
		}

		pb.Publish("primes", i)
		time.Sleep(1 * time.Second)
	}
}

func isPrime(value int) bool {
	for i := 2; i <= value/2; i++ {
		if value%i == 0 {
			return false
		}
	}

	return true
}
